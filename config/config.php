<?php

return [
    'components' => [
        'urlManager' => [
            'routes' => [
                '/'         => 'site/site/about',
                '/about'    => 'site/site/about',
                '/work'     => 'site/site/work',
                '/contact'  => 'site/site/contact',

//                '/vc'       => 'site/site/vc',
            ],
        ],
    ],
];
