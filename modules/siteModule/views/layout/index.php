<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>vCard - kaizerUA</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="shortcut icon" href="i/favicon.ico" type="image/x-icon">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://code.jquery.com/jquery-3.0.0.min.js" type="text/javascript"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js" type="text/javascript"></script>
<!--    Проверка на правильность ввода в поля формы обратной связи-->
    <script type="text/javascript">
        $(document).ready(function(){
            $("#contactform").validate();
        });
    </script>
</head>
<body>
    <div id="wrapper">
        <header>
            <div class="header-container">
                <div id="logo">
                    <p>Taras Lutsiuk</p>
                </div>
                <nav>
                    <ul class="site-nav">
                        <li><a href="about">about</a></li>
                        <li><a href="work">work</a></li>
                        <li><a href="contact">contact</a></li>
                    </ul>
                </nav>
            </div>
        </header>

        <main>
            <?= $content ?>
        </main>
    </div>
</body>
</html>