<!--block about vcard.dev-->
<div id="about">
    <div class="container">
        <img class="nav-shape-img" src="i/nav-shape.png" alt="">
        <div class="about-header">
            <img src="i/about-photo.jpg" alt="">
            <div class="text">
                <h2>Hello and Welcome</h2>
                <p>Let me introduce myself. I'm junior web developer. I'm working on my
                    project, which will give me the opportunity to go on an interesting job
                    in the IT. Now I'm learning HTML5, CSS, JavaScript and jQuery. This
                    vCard is my second job is that I'm using these tools. I have a framework
                    "Quick" that I created on PHP.
                </p>
            </div>
        </div>
        <div class="about-footer">
            <h2>My Skills</h2>
            <ul>
                <li>HTML, CSS</li>
                <li>JavaScript (jQuery library)</li>
                <li>Linux Ubuntu, MS Windows</li>
            </ul>
            <ul>
                <li>PhP, Yii2, MVC</li>
                <li>MySQL, InnoDB</li>
                <li>Git</li>
            </ul>
            <a class="btn" href="https://drive.google.com/file/d/0B-8Ys42CHWAUcnNxcWVEWEV3SVU/view?usp=sharing"
               target="_blank">Resume</a>
        </div>
    </div>
</div>