<?php
//Если форма отправлена
if(isset($_POST['submit'])) {
    //Проверка Поля ИМЯ
    if(trim($_POST['contactname']) == '') {
        $hasError = true;
    } else {
        $name = trim($_POST['contactname']);
    }
    //Проверка правильности ввода EMAIL
    if(trim($_POST['email']) == '')  {
        $hasError = true;
    } else if (!mb_eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['email']))) {
        $hasError = true;
    } else {
        $email = trim($_POST['email']);
    }
    //Проверка наличия ТЕКСТА сообщения
    if(trim($_POST['message']) == '') {
        $hasError = true;
    } else {
        if(function_exists('stripslashes')) {
            $comments = stripslashes(trim($_POST['message']));
        } else {
            $comments = trim($_POST['message']);
        }
    }
    //Если ошибок нет, отправить email
    if(!isset($hasError)) {
        $emailTo = 'kaizer@email.ua'; //на этот email будут приходить письма
        $subject = "Letter from {$_SERVER['HTTP_HOST']}. Someone want to contact with me.";
        $body = "Name: $name \n\nEmail: $email \n\nComments:\n $comments";
        $headers = "From: {$_SERVER['REDIRECT_URL']} <$emailTo>\r\nReply-To: $email\r\nX-Mailer: PHP/{phpversion()}\r\n"
            . "Content-Type: text/html; charset=\"utf-8\"\r\n";
        mail($emailTo, $subject, $body, $headers);
        $emailSent = true;
    }
}
?>
<!--block contact vcard.dev-->
<div id="contact">
    <div class="container">
        <img class="nav-shape-img" src="i/nav-shape.png" alt="">
        <div class="title">Let's keep in touch</div>
        <div class="contact-item">
            <form method="post" action="<?= $_SERVER['REDIRECT_URL']?>" id="contactform">
                <input type="text" name="contactname" id="contactname" placeholder="Name">
                <input type="text" name="email" id="email" placeholder="Email">
                <textarea cols="0" rows="0" name="message" id="message" placeholder="Message"></textarea>
                <input type="submit" value="Send Message" name="submit" class="btn">
            </form>
            <img class="vert-img" src="i/contact-vert.png" alt="">
            <div class="my-contact">
                <p>Name:</p>
                <p>Taras Lutsiuk</p>
                <p>Email:</p>
                <p>kaizer@email.ua</p>
                <p>Phone:</p>
                <p>+380977414877</p>
                <div class="social">
                    <a href="http://facebook.com/kaizerok">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-square-o fa-stack-2x"></i>
                            <i class="fa fa-facebook fa-stack-1x"></i>
                        &nbsp;</span>
                    </a>
                    <a href="https://bitbucket.org/crazycoders256/vcart-quick">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-square-o fa-stack-2x"></i>
                            <i class="fa fa-bitbucket fa-stack-1x"></i>
                        &nbsp;</span>
                    </a>
                    <a href="https://ua.linkedin.com/in/taras-lutsiuk-4a6b4bb0">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-square-o fa-stack-2x"></i>
                            <i class="fa fa-linkedin fa-stack-1x"></i>
                        &nbsp;</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>