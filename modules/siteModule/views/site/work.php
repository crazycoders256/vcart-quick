<!--block work vcard.dev-->
<div id="work">
    <div class="container">
        <img class="nav-shape-img" src="i/nav-shape.png" alt="">
        <div class="work-img">
            <div class="work-img-item">
                <a href="src/workOne/index.html">
                    <img src="i/work-img1.png" alt="">
                </a>
            </div>
            <div class="work-img-item">
                <a href="https://github.com/kaizerok/framework-quick">
                    <img src="i/work-img2.png" alt="">
                </a>
            </div>
            <div class="work-img-item">
                <a href="/about">
                    <img src="i/work-img3.png" alt="">
                </a>
            </div>
            <div class="work-img-item">
                <a href="#work">
                    <img src="i/work-img.png" alt="">
                </a>
            </div>
            <div class="work-img-item">
                <a href="#work">
                    <img src="i/work-img.png" alt="">
                </a>
            </div>
            <div class="work-img-item">
                <a href="#work">
                    <img src="i/work-img.png" alt="">
                </a>
            </div>
        </div>
        <div class="work-nav">
            <div class="work-nav-item"></div>
            <div class="work-nav-item"></div>
            <div class="work-nav-item"></div>
        </div>
    </div>
</div>